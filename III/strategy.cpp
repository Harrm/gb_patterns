
class IMemoryAllocator
{
public:
    virtual void setStrategy(IMemoryAllocationStrategy *strategy) = 0;
    virtual void *allocate(size_t size) = 0;
};

class MemoryAllocator : public IMemoryAllocator
{
public:
    virtual void setStrategy(IMemoryAllocationStrategy *strategy)
    {
        this->strategy = strategy;
    }

    virtual void *allocate(size_t size) override
    {
        return strategy->allocate(size);
    }

private:
    void *allocate_from_RAM_with_failure(size_t size)
    {
        // if no memory return nullptr
        return nullptr;
    }

    void *allocate_from_RAM_with_swap(size_t size)
    {
        auto *ptr = allocate_from_RAM_with_failure(size);
        if (ptr == nullptr)
        {
            return allocate_from_disk(size);
        }
        return ptr;
    }

    void *allocate_from_disk(size_t size)
    {
        // ...
    }

    IMemoryAllocationStrategy *strategy;
};

class IMemoryAllocationStrategy
{
public:
    virtual void *allocate(size_t size) = 0;
};

class RamWithFailureStrategy : public IMemoryAllocationStrategy
{
public:
    virtual void *allocate(size_t size)
    {
        // if failed to allocate
        std::terminate();
    }
};

class RamWithReserveStrategy : public IMemoryAllocationStrategy {
public:
    virtual void *allocate(size_t size)
    {
        // if failed to allocate
            reserveStrategy->allocate(size);
    }

private:
    IMemoryAllocationStrategy* reserveStrategy;
};

class DiskAllocateStrategy : public IMemoryAllocationStrategy {
