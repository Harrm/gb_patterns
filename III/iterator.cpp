#include <optional>
#include <iostream>
#include <memory>
#include <vector>
#include <functional>

// laaarge data
// lazy calculations
// container abstraction

template <std::copy_constructible T>
class IIterator
{
public:
    virtual void next() = 0;
    virtual bool isValid() const = 0;
    virtual std::optional<T> get() const = 0;

    virtual bool operator!=(IIterator<T> const& other) const = 0;

    virtual std::unique_ptr<IIterator<T>> clone() const = 0;
};

template <typename Iterator>
concept ForwardIterator = requires(Iterator it)
{
    {it++};
    {*it};
    {
        static_cast<bool>(it)
        } -> std::same_as<bool>;
    {
        it == it
        } -> std::same_as<bool>;
};

template <typename Container>
concept Iterable = requires(Container c)
{
    {
        std::begin(c)
        } -> ForwardIterator;
    {
        std::end(c)
        } -> ForwardIterator;
    {
        std::begin(c) == std::end(c)
        } -> std::same_as<bool>;
};

template <std::copy_constructible T>
T max_element(IIterator<T> const &iter)
{
    if(!iter.isValid()) return {};
    T max = iter.get().value();
    auto current = iter.clone();
    while (current->isValid())
    {
        auto element = current->get();
        if (element.value() > max) max = element.value();
        current->next();
    }
    return max;
}

class NumbersGenerator : public IIterator<int>
{
public:
    NumbersGenerator(int begin, int end) : current{begin}, kMax{end} {}

    virtual void next() override
    {
        current++;
    }

    virtual bool isValid() const override
    {
        return current < kMax;
    }

    virtual bool operator!=(IIterator<int> const& other) const {
        auto* other_casted = dynamic_cast<const NumbersGenerator*>(&other);
        return other_casted != nullptr || current != other_casted->current;
    }

    virtual std::optional<int> get() const override
    {
        if (isValid())
            return current;
        return std::nullopt;
    }

    virtual std::unique_ptr<IIterator<int>> clone() const override
    {
        return std::make_unique<NumbersGenerator>(current, kMax);
    }

private:
    int current = 0;
    const int kMax;
};

struct User
{
    int id;
};

struct Database
{
    User fetch_user(int id) const {}

    std::vector<User> get_all_users() const {
        return {};
    }

    struct DatabaseIterator : public IIterator<User>
    {
        virtual void next() override
        {
            // fetch next user
        }

        virtual bool isValid() const override
        {
            return true;
        }

        virtual std::optional<User> get() const override
        {
            return std::nullopt;
        }

        virtual bool operator!=(IIterator<User> const& other) const override {
            return false;
        }

        virtual std::unique_ptr<IIterator<User>> clone() const override
        {
            return nullptr;
        }
    };

    std::pair<IIterator<User> const&, IIterator<User> const&> get_all_users_range() const {
        DatabaseIterator begin, end;
        return {begin, end};
    }
};

void send_message(User user, std::string msg)
{
    //
}

void send_message_to_all_users_naive(Database &db, std::string msg)
{
    auto users = db.get_all_users();
    // we wait ages while all users are uploading
    // our app probably visibly freezes
    // and then we crash with OOM
    // ......
}

void send_message_to_all_users_proper(Database &db, std::string msg) {
    auto [begin, end] = db.get_all_users_range();
    auto current = begin.clone();
    while (current->isValid()) {
        auto user = current->get().value();
        send_message(user, "Hello, world!");
        current->next();
    }
}

class FunctionalGenerator : public IIterator<double>
{
public:
    FunctionalGenerator(double start_x, size_t steps_num, double x_step, std::function<double(double)> f) : x{start_x}, steps_left{steps_num}, kStep{x_step}, f{f} {}

    virtual void next() override
    {
        if(isValid()) {
            x += kStep;
            steps_left--;
        }
    }

    virtual bool isValid() const override
    {
        return steps_left > 0;
    }

    virtual std::optional<double> get() const override
    {
        return f(x);
    }

    virtual std::unique_ptr<IIterator<double>> clone() const override
    {
        return std::make_unique<FunctionalGenerator>(x, steps_left, kStep, f);
    }

    virtual bool operator!=(IIterator<double> const& other) const {
        auto* other_casted = dynamic_cast<const FunctionalGenerator*>(&other);
        return other_casted != nullptr || /* f != other_casted->f || */ x != other_casted->x;
    }

private:
    double x = 0;
    size_t steps_left;
    const double kStep;
    std::function<double(double)> f;
};

int main()
{
    NumbersGenerator it{1, 10000};
    while (it.isValid())
    {
        std::cout << it.get().value() << " ";
        it.next();
    }
    std::cout << "\n===================\n";

    FunctionalGenerator g1{-100.0, 200, 1.0, [](double x) { return x*x; }};
    std::cout << max_element(g1) << "\n";
    FunctionalGenerator g2{-100.0, 200, 1.0, [](double x) { return -x*x; }};
    std::cout << max_element(g2);
}