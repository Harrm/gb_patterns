
// third-party
// scoped interface realization
// smaller interfaces and decoupling

#include <vector>
#include <concepts>
#include <optional>
#include <memory>

template <std::copy_constructible T>
class IIterator
{
public:
    virtual void next() = 0;
    virtual bool isValid() const = 0;
    virtual std::optional<T> get() const = 0;

    virtual bool operator!=(IIterator<T> const& other) const = 0;

    virtual std::unique_ptr<IIterator<T>> clone() const = 0;
};

template <std::copy_constructible T>
class VectorIteratorAdapter: public IIterator<T> {
public:
    explicit VectorIteratorAdapter(std::vector<T>::iterator iter, std::vector<T>::const_iterator end): iter{iter}, kEnd{end}{}
    
    virtual void next() override {
        if(isValid()) iter++;
    }

    virtual bool isValid() const override {
        return iter < kEnd;
    }
    
    virtual std::optional<T> get() const override {
        if(isValid()) return *iter;
        return std::nullopt;
    }

    virtual bool operator!=(IIterator<T> const& other) const override {
        auto& other_casted = dynamic_cast<VectorIteratorAdapter<T>*>(&other);
        return other_casted == nullptr || iter != other_casted->iter;
    }

    virtual std::unique_ptr<IIterator<T>> clone() const override {
        return std::make_unique<VectorIteratorAdapter<T>>(iter);
    }

private:
    std::vector<T>::iterator iter;
    const typename std::vector<T>::const_iterator kEnd;
};
