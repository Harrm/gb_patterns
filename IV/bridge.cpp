#include <cstdint>
#include <iostream>

class Renderable {
  public:

    virtual void render() = 0;

  protected:
    Renderable(RenderableBackend* pimpl): pimpl{pimpl} {

    }

    void setColor(uint32_t rgba) {
      pimpl->setColor(rgba);
    }

    void renderLine(float x1, float y1, float x2, float y2) {
      pimpl->renderLine(x1, y1, x2, y2);
    }
    void renderRect(float x1, float y1, float x2, float y2) {
      for (auto y = y1; y < y2; y++) {

      }
    }
    void renderCircle(float x1, float y1, float r) {

    }

  private:
    RenderableBackend* pimpl;
};

class Plane: public Renderable {
  public:
    explicit Plane(RenderableBackend* backend): Renderable {backend} {}

    virtual void render() override {
      // render rect...
      // render line...
    };


};


class RenderableBackend {
  public:
    virtual void renderLine(float x1, float y1, float x2, float y2) = 0;
    virtual void setColor(uint32_t rgba) = 0;

  private:

};

class OstreamBackend: public RenderableBackend {
  public:
    explicit OstreamBackend(std::ostream& out) {}

    virtual void renderLine(float x1, float y1, float x2, float y2) override {
      // std::cout << ......
    }
    virtual void setColor(uint32_t rgba) override {
      // std::cout << control-sequence ........
    }

};

class VulkanBackend: public RenderableBackend{
  public:
    virtual void renderLine(float x1, float y1, float x2, float y2) override {
      // set line pipeline
      // command buffer <- draw line.....
    }
    virtual void setColor(uint32_t rgba) override {
      // set material......
    }
};

void foo() {
  auto backend = new OstreamBackend{std::cout};
  auto plane = new Plane{backend};
}