#include <memory>

class Crater {
  public:
    virtual void setPos(int) = 0;

};

class Ground {
  public:

    void onBombFalls(int pos) {
      auto crater = makeCrater();
      crater->setPos(pos);
    }

  protected:
    virtual std::unique_ptr<Crater> makeCrater() = 0;
};
