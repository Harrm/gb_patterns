
class CompilerImpl;

class Compiler {
 public:
  Compiler(CompilerImpl* pimpl) : pimpl{pimpl} {}

  void Build(std::vector<std::filesystem::path> files) {
    // read and compile all files
  }

  std::vector<std::byte> Compile(std::string code) {
    // in .cpp: return pimpl->compile(code);
  }

 private:
  CompilerImpl* pimpl;
};

enum class Platform { Windows, Mac, Linux } platform;
enum class Arch { amd64, M1, x86 } arch;

CompilerImpl* makeCompiler(Platform platform, Arch arch);

////// in .cpp

// #include everything

CompilerImpl* makeCompiler(Platform platform, Arch arch) {
  // ... create everything
  return new CompilerImpl{};
}
