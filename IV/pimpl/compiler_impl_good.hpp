#include <string>
#include <string_view>
#include <vector>

// #include "preprocessor_impl.hpp"
class Preprocessor {
  public:
    virtual std::string preprocess(std::string_view code) const = 0;
};

// #include "ast_builder_impl.hpp"
class Ast {};
class AstBuilder {
  public:
    virtual Ast build_ast(std::string_view code) const = 0;
};

// #include "ir_compiler_impl.hpp"
class Ir {};
class IrCompiler {
  public:
    virtual Ir compile_to_ir(Ast const& ast) const = 0;
};

// #include "optimizer_impl.hpp"
class Optimizer {
  public:
    virtual Ir optimize(Ir const& ir) const = 0;
};

// #include "assembler_impl.hpp"
class Assembler {
  public:
    virtual std::vector<std::byte> assemble(Ir const& ir) const = 0;
};

class CompilerImpl {
  public:
    std::vector<std::byte> compile(std::string code) {
      auto ir = optimizer->optimize(ir_compiler->compile_to_ir(ast_builder->build_ast(preprocessor->preprocess(code))));        
      return assembler->assemble(ir);
    }

  private:
    Preprocessor* preprocessor;
    AstBuilder* ast_builder;
    IrCompiler* ir_compiler;
    Optimizer* optimizer;
    Assembler* assembler;
};

class PreprocessorImpl: public Preprocessor {
public:
   virtual std::string preprocess(std::string_view code) const override { return ""; }
};

class AstBuilderImpl: public AstBuilder {
public:
   virtual Ast build_ast(std::string_view code) const override { return Ast{}; }
};

class IrCompilerImpl: public IrCompiler {
public:
       virtual Ir compile_to_ir(Ast const& ast) const override { return Ir{}; }
};

class OptimizerImpl: public Optimizer {
public:
       virtual Ir optimize(Ir const& ir) const override { return ir; }
};

class Windows_X64_Assebmler: public Assembler {
public:
       virtual std::vector<std::byte> assemble(Ir const& ir) const override { return {}; }
};

class Mac_M1_Assebmler: public Assembler {
public:
       virtual std::vector<std::byte> assemble(Ir const& ir) const override { return {}; }
};
