#include <string>
#include <string_view>
#include <vector>

// #include "preprocessor_impl.hpp"

class PreprocessorImpl {
public:
   virtual std::string preprocess(std::string_view code) const { return ""; }
};

// #include "ast_builder_impl.hpp"
class Ast {};

class AstBuilderImpl {
public:
   virtual Ast build_ast(std::string_view code) const { return Ast{}; }
};

// #include "ir_compiler_impl.hpp"
class Ir {};

class IrCompilerImpl {
public:
       virtual Ir compile_to_ir(Ast const& ast) const { return Ir{}; }
};

// #include "optimizer_impl.hpp"

class OptimizerImpl {
public:
       virtual Ir optimize(Ir const& ir) const { return ir; }
};

// #include "assembler_impl.hpp"

class Windows_X64_Assebmler {
public:
       virtual std::vector<std::byte> assemble(Ir const& ir) const { return {}; }
};

class Mac_M1_Assebmler {
public:
       virtual std::vector<std::byte> assemble(Ir const& ir) const { return {}; }
};

class Compiler {
  public:
    std::vector<std::byte> compile(std::string code) {
      auto ir = optimizer->optimize(ir_compiler->compile_to_ir(ast_builder->build_ast(preprocessor->preprocess(code))));
      if(platform == Platform::Windows && arch == Arch::amd64) {
        return windows_x64_assembler->assemble(ir);
      }
    }

  private:
    PreprocessorImpl* preprocessor;
    AstBuilderImpl* ast_builder;
    IrCompilerImpl* ir_compiler;
    OptimizerImpl* optimizer;
    Windows_X64_Assebmler* windows_x64_assembler;
    Mac_M1_Assebmler* mac_m1_assembler;
    enum class Platform { Windows, Mac, Linux } platform;
    enum class Arch { amd64, M1, x86 } arch;
};
