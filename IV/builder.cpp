#include <iostream>
#include <string>
#include <string_view>

struct Font {};

class TextConverter {
 public:
  virtual void addChar(char c) = 0;
  virtual void addParagraphBreak() = 0;
  virtual void addFontChange(Font const& font) = 0;
};

class PlainTextConverter : public TextConverter {
 public:
  virtual void addChar(char c) { text += c; }

  virtual void addParagraphBreak() { text += "\n"; }

  virtual void addFontChange(Font const& font) { ; }

  std::string getText() const { return text; }

 private:
  std::string text;
};

// class TextWidgetConverter: public TextConverter {....

enum class CharType { Text, ParagraphBreak, FontChange };

CharType getType(char c) { return {}; }

class RtfReader {
 public:
  void parseRtf(std::string_view rtf, TextConverter& converter) {
    for (char c : rtf) {
      switch (getType(c)) {
        case CharType::Text:
          converter.addChar(c);
          break;
        case CharType::ParagraphBreak:
          converter.addParagraphBreak();
          break;
        case CharType::FontChange:
          converter.addFontChange(Font{});
          break;
      }
    }
  }
};

int main_1() {
  RtfReader reader;
  PlainTextConverter converter;
  reader.parseRtf("....", converter);
  std::cout << converter.getText();
  return 0;
}

class AbstractProduct {};

class AbstractProductBuilder {
 public:
  class Template {
   public:
    virtual Template& set_option_1(int value) = 0;
    virtual Template& set_option_2(int value) = 0;
    virtual std::unique_ptr<AbstractProduct> build() && = 0;
  };
  virtual std::unique_ptr<Template> start() = 0;
};

class ProductImpl_1 : public AbstractProduct {
 public:
  ProductImpl_1(int, int) {}

  int one;
  int two;
};

class Product1_Bulder : public AbstractProductBuilder {
 public:
  class Product1_Template : public Template {
   public:
    Template& set_option_1(int value) override {
      option_1 = value;
      return *this;
    }

    Template& set_option_2(int value) override {
      option_2 = value;
      return *this;
    }

    virtual std::unique_ptr<AbstractProduct> build() && override {
      return std::make_unique<ProductImpl_1>(option_1, option_2);
    }

   private:
    int option_1;
    int option_2;
  };
  virtual std::unique_ptr<Template> start() {
    return std::make_unique<Product1_Template>();
  }
};

void bar(AbstractProductBuilder::Template& t) {
  t.set_option_1(32);
  t.set_option_2(42);
}

int main() {
  Product1_Bulder builder;
  auto t = builder.start();
  bar(*t);
  auto product = std::move(*t).build();

  return 0;
}
