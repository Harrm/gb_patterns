#include <map>
#include <iostream>

class IDatabase {
 public:
  virtual std::pair<int, bool> get(int k) = 0;

  [[nodiscard]] virtual bool set(int k, int v) = 0;
};

class PostgreSqlDatabase : public IDatabase {
 public:
  virtual std::pair<int, bool> get(int k) {
    // long and expensive database access
    return std::make_pair(k * 42, true);
  }

  [[nodiscard]] virtual bool set(int k, int v) {
    // long and expensive database access
    return true;
  }
};

void process_user_input(IDatabase& db) {
  auto [value, success] = db.get(11);
  if (!success) {
    std::cout << "First get failed!\n";
    return;
  }
  success = db.set(11, 44);
  if (!success) {
    std::cout << "First set failed!\n";
    return;
  }
  auto [value1, success1] = db.get(11);
  if (!success1) {
    std::cout << "Second get failed!\n";
    return;
  }
}

int main1() {
  PostgreSqlDatabase db;
  process_user_input(db);
  return 0;
}

/////////////////////////////////////////////////

class CachedDatabase : public IDatabase {
 public:
  CachedDatabase(IDatabase* inner) : inner{inner} {}

  virtual std::pair<int, bool> get(int k) {
    if (cache.find(k) != cache.end()) {
      cache[k] = v;
      return std::make_pair(cache[k], true);
    }
    return inner->get(k);
  }

  [[nodiscard]] virtual bool set(int k, int v) {
    cache[k] = v;
    return inner->set(k, v);
  }

 private:
  IDatabase* inner;
  std::map<int, int> cache;
};

int main2() {
  PostgreSqlDatabase db;
  CachedDatabase cdb{&db};
  process_user_input(cdb);
  return 0;
}

/////////////////////////////////////////////

class SecureDatabase : public IDatabase {
 public:
  SecureDatabase(IDatabase* inner) : inner{inner} {}

  void login(int uid) { current_user = uid; }

  virtual std::pair<int, bool> get(int k) { return inner->get(k); }

  [[nodiscard]] virtual bool set(int k, int v) {
    if (current_user != 0) {
      return false;
    }
    return inner->set(k, v);
  }

 private:
  IDatabase* inner;
  int current_user;
};

int main() {
  PostgreSqlDatabase db;
  CachedDatabase cdb{&db};
  SecureDatabase sdb{&cdb};
  sdb.login(0);
  process_user_input(sdb);
}
