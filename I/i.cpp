
// BAD

class UserRepo_Bad {
public:
    virtual void set_user(int id, int user);
    virtual int get_user(int id);
    virtual bool has_user(int id);

    virtual void set_role(int user_id, int role);
    virtual int get_role(int user_id);
    virtual int get_permissions(int role);

    virtual int* get_recommendations(int user_id);
};

// GOOD

class UserRepo {
    virtual void set_user(int id, int user);
    virtual int get_user(int id);
    virtual bool has_user(int id);
};


class RoleRepo {
    virtual void set_role(int user_id, int role);
    virtual int get_role(int user_id);
    virtual int get_permissions(int role);
};

class RecommendationsRepo {
    virtual int* get_recommendations(int user_id);
};
