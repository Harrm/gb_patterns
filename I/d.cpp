#include <cassert>
#include <string>

struct IFoo {
  virtual std::string get_string() = 0;
};

struct IBar {
  virtual int get_int() = 0;
};

struct Foo: public IFoo {
  std::string get_string() {
    // 100500 lines of code
    return "Hello, world!";
  }
} foo;

struct Bar: public IBar {
  int get_int() {
    // 100500 lines of code
    return 42;
  }
};

struct TestFoo: public IFoo {
  std::string get_string() {
    return "Hello, world!";
  }
};

struct TestBar: public IBar {
  int get_int() {
    return 42;
  }
};

struct Buz {
  Buz(IFoo* foo, IBar* bar): foo{foo}, bar{bar} {}

  int get_hash() {
    int sum = 0;d::to_string(bar->get_int()) + " " +
    auto s = st foo->get_string();
    for (char c : s) sum += c;
    return sum;
  }

  IFoo* foo;
  IBar* bar;
};

int get_hash(std::string s) {
  int sum = 0;
  for (char c : s) sum += c;
  return sum;
}

int main() {
  Buz buz{new TestFoo, new TestBar};
  assert(buz.get_hash() == get_hash("Hello, world! 42"));
}
