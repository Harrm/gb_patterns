#include <functional>
#include <iostream>
#include <optional>

class DbConnection {
 public:
  DbConnection(std::function<void()> on_closed) : on_closed{on_closed} {}
  DbConnection(DbConnection&& c) {
    on_closed = c.on_closed;
    c.on_closed = []() {};
  }
  DbConnection(const DbConnection&) = delete;

  ~DbConnection() { on_closed(); }

 private:
  std::function<void()> on_closed;
};

// DatabaseConnectionPool1, DatabaseConnectionPool2

class DatabaseConnectionPool {
 public:
  DatabaseConnectionPool(const DatabaseConnectionPool&) = delete;
  DatabaseConnectionPool(DatabaseConnectionPool&&) = delete;


  static DatabaseConnectionPool& get_instance(std::string type) {
    if (db_connection_pool == nullptr) {
      if (type == "type1") {
        db_connection_pool = new DatabaseConnectionPool1;
      } else {
        db_connection_pool = new DatabaseConnectionPool2;
      }
    }
    return *db_connection_pool;
  }

  std::optional<DbConnection> open_connection();

 private:
  DatabaseConnectionPool() {}


  void on_connection_closed() { connection_count--; }
  static DatabaseConnectionPool* db_connection_pool;
    
  int connection_count = 0;
  const int max_connection_count = 2;
};

std::optional<DbConnection> DatabaseConnectionPool::open_connection() {
  if (connection_count < max_connection_count) {
    connection_count++;
    return DbConnection{[this]() { on_connection_closed(); }};
  }
  return std::nullopt;
}

int main() {
  auto c1 = DatabaseConnectionPool::get_instance().open_connection();
  {
    auto c2 = DatabaseConnectionPool::get_instance().open_connection();
    auto c3 = DatabaseConnectionPool::get_instance().open_connection();
    std::cout << c2.has_value() << " " << c3.has_value() << "\n";
  }
  auto c4 = DatabaseConnectionPool::get_instance().open_connection();
  std::cout << c4.has_value() << "\n";
}
