
class GameManager { // BAD
public:
    void resolve_collisions();
    void check_collisions();
    void render_frame();
    void load_textures();
    void check_is_game_over();
    void save_game();
    void check_user_scores();
    void resolve_forces();
};

// GOOD

class RenderScene {
    void load_textures();
};

class RenderSystem {
    void render_frame();

    RenderScene* scene;
};

class CollisionSolver {
    void resolve_collisions();
    void check_collisions();
};

class PhysicsSystem {
    void resolve_forces();

    CollisionSolver* solver;
};

class SaveSystem {
    void save_game();
};

class GameMode {
    void check_is_game_over();
    void check_user_scores();
};
