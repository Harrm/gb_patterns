#include <cassert>
#include <string>

class Animal {
public:
    virtual void put_collar(std::string name) = 0;
    virtual std::string read_name() = 0;
};

void some_f(Animal* a) {
    a->put_collar("Fido");
    assert(a->read_name() == "Fido");
}

class Cat: public Animal {
public:
    virtual void put_collar(std::string name) {
        this->name = name;
    }

    virtual std::string read_name() {
        return name;
    }

private:
    std::string name;
};

class Fish: public Animal {
public:
    virtual void put_collar(std::string name) {
    }

    virtual std::string read_name() {
        return "A fish";
    }

};
