#include <iostream>

// Bad
class Animal_Bad {
    enum class Kind {
        Cat, Fish, Dog
    };

    void make_sound() {
        switch(kind) {
            case Kind::Cat:
            std::cout << "Meow\n";
            case Kind::Dog:
            std::cout << "Bark\n";
            case Kind::Fish:
            ;
        }
    }

    Kind kind;
};

// Good

class Animal {
public:
    virtual void make_sound() = 0;
};

class Cat: public Animal {
public:
    virtual void make_sound() override {
        std::cout << "Meow\n";
    }
};

class Fish: public Animal {
public:
    virtual void make_sound() override {
    }
};

// Bad
class Person {
public:
    void pet(Animal* animal) {
        if (dynamic_cast<Cat*>(animal) == nullptr) {
            std::cout << "The cat says: Hiss\n";
            
        } else if(dynamic_cast<Fish*>(animal) == nullptr) {
            std::cout << "You can't pet a fish\n";
        }
    }

};
