
class Unit {
public:
    virtual ~Unit() = default;

    virtual void receive_damage(int amount) {}
    virtual void draw() {}
};

class Trooper: public Unit {
    virtual void walk() {}
};

class Tank: public Unit {
    virtual void shoot_cannon() {}
};

class Airbourne: public Unit {
    virtual void drop_bomb() {}
};

class RedTrooper: public Trooper {
    RedTrooper(int health) {}
};

class RedTank: public Tank {
    RedTank(int fuel, bool has_machine_gun) {}
};

class RedAirbourne: public Airbourne {
    RedAirbourne(int max_height, int fuel) {}
};

class WhiteTrooper: public Trooper {
    WhiteTrooper(int health, int shield) {}
};

class WhiteTank: public Tank {
    WhiteTank(int crew_size, int guns_num) {}
};

class WhiteAirbourne: public Airbourne {
    WhiteAirbourne(int max_speed, bool has_turbo) {}
};
