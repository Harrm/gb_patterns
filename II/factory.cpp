
#include <map>
#include <string>
#include <string_view>
#include <vector>
#include <memory>

#include "common.hpp"

enum class Fraction { Red, White };

class GameState {
 public:
  Fraction current_player;
  std::vector<std::unique_ptr<Unit>> units;
};

class InputController_First {
  void process_input(std::string command, std::vector<std::string> args) {
    if (command == "spawn") {
      if (args.at(1) == "trooper") {
        if (state.current_player == Fraction::Red) {
          // calculate all params
          state.units.push_back(std::make_unique<RedTrooper>(42));
        } else {
          // calculate all params
          state.units.push_back(std::make_unique<WhiteTrooper>(42));
        }
      } else if (args.at(1) == "tank") {
        // ...
      } else if (args.at(1) == "airbourne") {
        // ...
      }
    }
  }

  // over9000 gameplay classes needed to calculate construction params
  // ... and 100500 for other commands
  GameState& state;
};

enum class UnitType { TROOPER, TANK, AIRBOURNE };

UnitType str_to_unit_type(std::string);

class InputController_Second {
 public:
  void add_command(std::string name, Command* command) {
    commands.insert({name, command});
  }

  void process_input(std::string command,
                     const std::vector<std::string>& args) {
    auto cmd = commands["spawn"];
    if (command == "spawn") {
      auto spawn_cmd = dynamic_cast<SpawnUnitCommand*>(cmd);
      spawn_cmd->setArgs({str_to_unit_type(args[0]), state.current_player,
                          stoi(args[1]), stoi(args[2])});
    }
    // ...
    cmd->execute();
  }

  std::map<std::string, Command*> commands;

  GameState& state;
};

class Command {
 public:
  virtual ~Command() = default;
  virtual void execute() = 0;
};

// two reasons to change the class
// N*M scaling
// M dependency sets
class SpawnUnitCommand: public Command {
 public:
  struct Args {
    UnitType type;
    Fraction fraction;
    int first_param;
    int second_param;
  };
  SpawnUnitCommand(GameState& state /*, over 9000 game dependencies*/)
      : state{state} {}

  void setArgs(Args args) { this->args = args; }

  void execute() {
    if (args.type == UnitType::TROOPER) {
      if (args.fraction == Fraction::Red) {
        // ...calculate all params
        state.units.push_back(std::make_unique<RedTrooper>(args.first_param));
      } else {
        // ...calculate all params
        state.units.push_back(std::make_unique<WhiteTrooper>(args.first_param));
      }
    } else if (args.type == UnitType::TANK) {
      // ...
    } else if (args.type == UnitType::AIRBOURNE) {
      // ...
    }
  }

  Args args;
  // over 9000 game dependencies
  GameState& state;
};

class UnitFactory {
 public:
  virtual std::unique_ptr<Trooper> makeTrooper(/* params */) = 0;
  virtual std::unique_ptr<Tank> makeTank(/* params */) = 0;
  virtual std::unique_ptr<Airbourne> makeAirbourne(/* params */) = 0;
};

class RedUnitFactory: public UnitFactory {
 public:
   virtual std::unique_ptr<Trooper> makeTrooper(/* params */) {
     // calculate game-related params
     return std::make_unique<RedTrooper>(/*params*/);
   }
  virtual std::unique_ptr<Tank> makeTank(/* params */) {
     // calculate game-related params
     return std::make_unique<RedTank>(/*params*/);
  }
  virtual std::unique_ptr<Airbourne> makeAirbourne(/* params */) {
     // calculate game-related params
     return std::make_unique<RedAirbourne>(/*params*/);
  }

  // (over 9000) / 2 game dependencies
  GameState& state;
};

class SpawnUnitCommand_Second {
 public:
  struct Args {
    UnitType type;
    Fraction fraction;
    int first_param;
    int second_param;
  };
  SpawnUnitCommand_Second(GameState& state)
      : state{state} {}

  void setArgs(Args args) { this->args = args; }

  void execute() {
    if (args.type == UnitType::TROOPER) {
      // ...calculate input-related params
      state.units.push_back(factories[args.fraction]->makeTrooper(/* params */));
    } else if (args.type == UnitType::TANK) {
      // ...calculate input-related params
      state.units.push_back(factories[args.fraction]->makeTank(/* params */));
    } else if (args.type == UnitType::AIRBOURNE) {
      // ...calculate input-related params
      state.units.push_back(factories[args.fraction]->makeAirbourne(/* params */));
    }
  }

  Args args;
  std::map<Fraction, UnitFactory*> factories;
  GameState& state;
};

class Unit_Second {
  // damage modifiers, render modifiers, movement modifiers, .......
};

class ForceFieldUnit: public Unit {
public:
  void receive_damage(int amount) {
    wrapped_unit->receive_damage(amount / 2);
  }

  void draw() {
    wrapped_unit->draw();
    draw_force_field();
  }

private:
  void draw_force_field();
  Unit* wrapped_unit;
};

int main() {}
